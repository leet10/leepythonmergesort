# main.py
# Reads numbers from a specified filename to sort
# using merge sort using the module mergesort.py
# and writes to a new file along with printing stats: input size, comparisons, assignments, and time.
#
# Usage: python3 main.py filename
#
# Created - January 2022 - Tou Ko Lee
# Updated - February 2022 - Tou Ko Lee

import sys
import os
from mergesort import sort 

def writeFile(n):
    """ Reads a list of numbers from the specified file or folder 
    and writes to a new file with the same name with "-sorted" 
    appended to the end of the new file.

    Parameters
    ----------
    n : string
        The name of the file to sort the numbers
    """
    file = n
    array = []

    # opens the file and reads the numbers to be sorted
    with open(file, 'r') as f:
        while(True):
            numString = f.readline()
            if (numString == ''):
                break
            num = int(numString)  
            array.append(num)

    # helps with formating when printing the stats
    print("Sorting:", file)
    sort(array)
    print()
       
    # creates new file and renames it with a similar name
    file2 = file.split(".")
    file2 = file2[0] + "-sorted." + file2[1]

    # writes the sorted array into the new file
    with open(file2, 'w') as f:
        for num in array:
            num = str(num)
            f.write(num + "\n")

def getFolder(n):
    """ Changes directory to get list of files to sort and sorts the numbers in them

    Parameters
    ----------
    n : string
        The name of the folder to change directory to
    """
    os.chdir(n)
    for file in os.listdir():
        if file.endswith(".txt"):
            if file.endswith("-sorted.txt"):
                continue
            writeFile(file)

def main():
    """ Reads a list of numbers from the specified file or folder 
    and writes to a new file with the same name with "-sorted" 
    appended to the end of the new file.
    
    sys.argv[1] : The name of the file to be sorted are specified by command line arguments.
    """
    name = sys.argv[1]
    if name.endswith(".txt"):
        writeFile(name)
    else:
        getFolder(name)


if __name__ == "__main__" :
    main()
