# mergesort.py
# Module used to sort numbers using merge sort and prints out stats: input size, comparisons, assignments, and time.
#
# Usage: needs to be imported to a file to be used
#
# Created - January 2022 - Tou Ko Lee
# Updated - February 2022 - Tou Ko Lee

from cmath import inf
import time

def sort(A):
    """ Calls merge-sort() and keeps track of stats
        
    Stats are printed when the arrays are sorted for the full array, 
    half the array, and a quarter of the array. The stats that are printed are: 
    input size, comparisons, assignments, and time (in seconds).
        
    Parameters
    ----------
    A : array
        The array you want to sort
    """

    # creating a temp array to hold a fixed amount of numbers
    temp = [] * int((len(A)/4))
    for i in range(int((len(A)/4))):
        temp.append(A[i])
    start_time = time.time()
    # comp is the number of comparisons in merge-sort and in merge
    # assign is the number of assignments in merge when placing 
    # the numbers from the left and right array to A[]
    comp, assign = merge_sort(temp, 0, len(temp)-1)
    end_time = time.time()
    # time elapsed from start to end of merge_sort()
    t = end_time - start_time
    print("numbers sorted: %-10s comparisons: %-10s assignments: %-10s time: %-20s seconds" % (len(temp), comp, assign, t))

    temp = [] * int((len(A)/2))
    for i in range(int((len(A)/2))):
        temp.append(A[i])
    start_time = time.time()
    comp, assign = merge_sort(temp, 0, len(temp)-1)
    end_time = time.time()
    t = end_time - start_time
    print("numbers sorted: %-10s comparisons: %-10s assignments: %-10s time: %-20s seconds" % (len(temp), comp, assign, t))

    start_time = time.time()
    comp, assign = merge_sort(A, 0, len(A) - 1)
    end_time = time.time()
    t = end_time - start_time
    print("numbers sorted: %-10s comparisons: %-10s assignments: %-10s time: %-20s seconds" % ((len(A)), comp, assign, t))

    
def merge_sort(A, p, r):
    """ Recursive function to sort numbers using merge sort
        
    The list is sorted in-place.
        
    Parameters
    ----------
    A : array
        The array you want to sort
    p : int
        The starting position of the array
    r : int
        The last index of the array

    Returns
    -------
    int
        The number of comparisons from merge-sort and merge
    int
        The number of assignments from merge, when placing the left and right elements into A[]
    """
    comp = 0
    assign = 0
    # Checks if 1 or more numbers are needed to be to sorted
    if (p<r):
        comp += 1
        # establish middle index
        q = int((p+r)/2)
        
        # sort left half
        fcomp, fassign = merge_sort(A,p,q)
        comp += fcomp
        assign += fassign

        # sort right half
        fcomp, fassign = merge_sort(A,q+1,r)
        comp += fcomp
        assign += fassign

        # merge left and right half
        fcomp, fassign = merge(A,p,q,r)
        comp += fcomp
        assign += fassign
    return comp, assign

def merge(A,p,q,r):
    """ Merges left and right subarrays
        
    Parameters
    ----------
    A : array
        The array you are sorting
    p : int
        The starting index of the array
    q : int
        The middle index of the array
    r : int
        The last index of the array

    Returns
    -------
    int
        The number of comparisons from merge-sort and merge
    int
        The number of assignments from merge, when placing the left and right elements into A[]
    """
    comp = 0
    assign = 0

    # size for left and right subarrarys
    n1 = q - p + 1
    n2 = r - q
   
    # fills left and right subarrays with infinites to be compared later on
    left = [inf] * (n1)
    right = [inf] * (n2)
    
    # filling the left and right subarrays
    for i in range(n1):
        left[i] = A[p + i]

    for j in range(n2):
        right[j] = A[q + j + 1]
    
    # index into the left and right subarrays to mark next element to merge.
    i = 0
    j = 0
    k = p

    # iterate over the subarray from A[p] to A[r]
    while i < n1 and j < n2:
        if left[i] <= right[j]:
            A[k] = left[i]
            i += 1
        else:
            A[k] = right[j]
            j += 1
        k += 1
        assign += 1
        comp +=1

    # Copy the remaining numbers of left[], if there are any
    while i < n1:
        A[k] = left[i]
        i += 1
        k += 1
        assign += 1
        comp +=1
 
    # Copy the remaining numbers of right[], if there are any
    while j < n2:
        A[k] = right[j]
        j += 1
        k += 1
        assign += 1
        comp +=1
        
    return comp, assign